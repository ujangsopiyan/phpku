<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>belajar bootstrap</title>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  </head>
  <body>
      <div class="container">
          <!--atas-->
            <div class="row">
               <div class="col-sm-3">&nbsp;</div>
               <div class="col-sm-6">
                   <nav class="navbar navbar-default">
                    <div class="container-fluid">
                      <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">WebSiteName</a>
                      </div>
                      <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                          <li class="active"><a href="index2.php?pg=vhitungbbideal">Hitung Berat Badan Ideal</a></li>
                          <li><a href="index2.php?pg=vhitunggaji">Hitung Gaji (jam kerja dalam seminggu)</a></li>
                          <li><a href="index2.php?pg=vhitungusia">Hitung Usia (dalam tahun)</a></li>
                          <li><a href="index2.php?pg=vhitungtahun">Hitung Tahun</a></li>
                          <li><a href="index2.php?pg=vhitunggaji2">Hitung Gaji 2 (jam kerja dalam seminggu</a></li>
                          <li class="dropdown">
                          <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1
                          <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Menu Home</a></li>
                            <li><a href="#">Menu Contact</a></li>
                            <li><a href="#">Menu Login</a></li>
                          </ul>
                        </li>
                        </ul>


                      </div>
                    </div>
                  </nav>
               </div>
               <div class="col-sm-3">&nbsp;</div>
            </div>
            <!--atas akhir-->
            
            <div class="row">
               <div class="col-sm-3">&nbsp;</div>
               <div class="col-sm-6">
                   
                   <!--konten disini-->
                    <div class="panel panel-default">
                        <!--koding php-->
                            <?php
                            if(isset($_GET['pg']))
                            {
                                //view* panggil vhitungbbideal.php
                                if($_GET['pg'] == 'vhitungbbideal')
                                {
                                    include 'vhitungbbideal.php';
                                }
                                //view* panggil vhitungsuhu.php
                                elseif($_GET['pg'] == 'vhitungsuhu')
                                {
                                    include 'vhitungsuhu.php';
                                }
                                //view* panggil vhitungusia.php
                                elseif($_GET['pg'] == 'vhitungusia')
                                {
                                    include 'vhitungusia.php';
                                }
                                //view* panggil vhitunggaji.php
                                elseif($_GET['pg'] == 'vhitunggaji')
                                {
                                    include 'vhitunggaji.php';
                                }
                                //view* panggil vhitungtahun
                                elseif($_GET['pg'] == 'vhitungtahun')
                                {
                                    include 'vhitungtahun.php';
                                }
                                elseif($_GET['pg'] == 'vhitunggaji2')
                                {
                                    include 'vhitunggaji2.php';
                                }
                                // proses* panggil phitungbbideal.php
                                elseif($_GET['pg'] == 'phitungbbideal')
                                {
                                    include 'phitungbbideal.php';
                                }
                                //proses* panggil phitungusia
                                elseif($_GET['pg'] == 'phitungusia')
                                {
                                    include 'phitungusia.php';
                                }
                                //proses* panggil phitunggaji.php
                                elseif($_GET['pg'] == 'phitunggaji')
                                {
                                    include 'phitunggaji.php';
                                }
                                //proses* panggil phitungtahun
                                elseif($_GET['pg'] == 'phitungtahun')
                                {
                                    include 'phitungtahun.php';
                                }
                                //proses* panggil phitunggaji2
                                elseif($_GET['pg'] == 'phitunggaji2')
                                {
                                    include 'phitunggaji2.php';
                                }
                            }
                            else
                            {
                                include 'vhome.php';
                            }
                            ?>
                        <!--koding php akhir-->
                    </div>
                   <!--konten akhir-->
               </div>
               <div class="col-sm-3">&nbsp;</div>
            </div>
            <!--tengah akhir-->
            
            <!--footer awal-->
            <div class="row">
            <div class="col-sm-3">&nbsp;</div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <!--<div class="panel-body">Panel Content</div>-->
                    <div class="panel-footer">&copy; ujang sopiyan</div>
                </div>
            </div>
            <div class="col-sm-3">&nbsp;</div>
            </div>
            <!--footer akhir-->
      </div>
      
      
      
      
<!--
      
 <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">Page 1</a></li>
        <li><a href="#">Page 2</a></li>
        <li><a href="#">Page 3</a></li>
        <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1
        <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="#">Page 1-1</a></li>
          <li><a href="#">Page 1-2</a></li>
          <li><a href="#">Page 1-3</a></li>
        </ul>
      </li>
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
    </div>
  </div>
</nav>-->
        <script src="bootstrap/jquery/jquery-3.1.1.min.js" charset="utf-8"></script>
       <script src="bootstrap/js/bootstrap.min.js" charset="utf-8"></script>
  </body>
</html>
