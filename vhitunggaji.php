<div class="panel-heading">Aplikasi Hitung Gaji (dalam jam kerja)</div>
    <div class="panel-body">
        <form role="form" method="post" action="index.php?pg=phitunggaji">
            <div class="form-group">
              <label for="">Jam kerja :</label>
              <input type="text" class="form-control" id="" name="jamkerja" placeholder="contoh: 48 jam" required> 
            </div>
            <div class="form-group">
              <label for="">Gaji Per Jam :</label>
              <input type="text" class="form-control" id="" name="gaji_perjam" placeholder="contoh: Rp. 15.000" required> 
            </div>
            <div class="form-group">
              <label for="">Gaji Lemburan :</label>
              <input type="text" class="form-control" id="" name="gaji_lemburan" placeholder="contoh: Rp. 20.000" required> 
            </div>
              <button type="submit" class="btn btn-primary" name="btnSubmit">Hitung</button>
        </form>
     </div>