<div class="panel-heading">Aplikasi Hitung Luas (persegi panjang)</div>
    <div class="panel-body">
        <form role="form" method="post" action="index.php?pg=phitungpersegipanjang">
            <div class="form-group">
                <label for="">Panjang : </label>
              <input type="text" class="form-control" id="" name="panjang" placeholder="contoh: 10" required> 
            </div>
            <div class="form-group">
                <label for="">Lebar : </label>
              <input type="text" class="form-control" id="" name="lebar" placeholder="contoh: 5" required> 
            </div>
            
            <button type="submit" name="btnSubmit" class="btn btn-primary">Hitung</button>
        </form>
     </div>

