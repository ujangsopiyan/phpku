<div class="panel-heading">Aplikasi Hitung Gaji (jam kerja dalam seminggu)</div>
    <div class="panel-body">
        <form role="form" method="post" action="index.php?pg=phitunggaji2">
            <div class="form-group">
                <label for="">Jam kerja (dalam seminggu): </label>
              <input type="text" class="form-control" id="" name="jamkerja" placeholder="contoh: 48 jam" required> 
            </div>
            <div class="form-group">
              <label for="">Golongan Karyawan :</label>
              <select name="golongan" id="" class="form-control">
                  <option value="">--Pilih--</option>
                  <option value="Golongan A">Golongan A</option>
                  <option value="Golongan B">Golongan B</option>
                  <option value="Golongan C">Golongan C</option>
                  <option value="Golongan D">Golongan D</option>
              </select>
            </div>
            <button type="submit" name="btnSubmit" class="btn btn-primary">Hitung</button>
        </form>
     </div>

