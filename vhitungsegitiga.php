<div class="panel-heading">Aplikasi Hitung Luas (segitiga)</div>
    <div class="panel-body">
        <form role="form" method="post" action="index.php?pg=phitungsegitiga">
            <div class="form-group">
              <label for="">Panjang/Alas :</label>
              <input type="text" class="form-control" id="" name="panjang" placeholder="contoh: 4 cm" required> 
            </div>
            <div class="form-group">
              <label for="">Tinggi :</label>
              <input type="text" class="form-control" id="" name="tinggi" placeholder="contoh: 3 cm" required> 
            </div>
              <button type="submit" class="btn btn-primary" name="btnSubmit">Hitung</button>
        </form>
     </div>
